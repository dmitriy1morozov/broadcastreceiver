package com.example.broadcastreceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.broadcastreceiver.MainActivity.Companion.KEY_DATA

class ReceiverSticky : BroadcastReceiver() {

    var mCounter = 0

    companion object {
        val TAG = ReceiverSticky::class.java.simpleName
    }

    override fun onReceive(context: Context, intent: Intent) {
        val strData = intent.extras?.getString(KEY_DATA)
        Log.d(TAG, "onReceive: Receiver = ${this@ReceiverSticky.hashCode()}")
        mCounter++
        Log.d(TAG, "onReceive: strData = $strData, mCounter = $mCounter; executing...")

        if (isInitialStickyBroadcast) {
            if (mCounter > 3) {
                mCounter = 0
                Log.d(TAG, "onReceive: Sticky Broadcast is removed from the system")
                context.removeStickyBroadcast(intent)
            }
        }

        Log.d(TAG, "onReceive: strData = $strData, executing FINISHED")
    }
}