package com.example.broadcastreceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.broadcastreceiver.MainActivity.Companion.KEY_DATA

class ReceiverStatic : BroadcastReceiver() {

    companion object {
        val TAG = ReceiverStatic::class.java.simpleName
    }

    override fun onReceive(context: Context, intent: Intent) {
        val strData = intent.extras?.getString(KEY_DATA)
        Log.d(TAG, "onReceive: strData = $strData")
    }
}