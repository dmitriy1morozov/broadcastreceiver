package com.example.broadcastreceiver

import android.content.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.localbroadcastmanager.content.LocalBroadcastManager


class MainActivity : AppCompatActivity() {

    val mReceiverSticky = ReceiverSticky()

    val mReceiver1 = Receiver1()
    val mReceiver2: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val strData = intent?.extras?.getString(KEY_DATA)
            Log.d("Receiver2", "onReceive: strData = $strData")

            if (isOrderedBroadcast) {
                Log.d(
                    "Receiver2",
                    "onReceive: orderedBroadcast resultCode = $resultCode, resultData = $resultData"
                )
                // TODO: 11/4/2021 Stop the ordered broadcast chain
//                abortBroadcast()
            }
        }
    }

    companion object {
        val KEY_DATA = "data_to_send"

        //        val ACTION_RECEIVER_STATIC = "my.static.receiver"
        val ACTION_RECEIVER_1 = "my.dynamic.receiver_1"
        val ACTION_RECEIVER_STICKY = "my.sticky.receiver"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        findViewById<Button>(R.id.btnReceiverStatic).setOnClickListener {
//            val dataToSend = "Default data"
//
//            val intent = Intent(ACTION_RECEIVER_STATIC).putExtra(KEY_DATA, dataToSend)
//            val infos = packageManager.queryBroadcastReceivers(intent, 0)
//            for (info in infos) {
//                val cn = ComponentName(
//                    info.activityInfo.packageName,
//                    info.activityInfo.name
//                )
//                intent.component = cn
//                sendBroadcast(intent)
//            }
//        }


        findViewById<Button>(R.id.btnBroadcastToDynamic).setOnClickListener {
            sendBroadcast(Intent(ACTION_RECEIVER_1).putExtra(KEY_DATA, "dataToSend"))
        }

        findViewById<Button>(R.id.btnOrderedBroadcast).setOnClickListener {
            sendOrderedBroadcast(Intent(ACTION_RECEIVER_1).putExtra(KEY_DATA, "dataToSend"), null)
        }

        findViewById<Button>(R.id.btnLocalBroadcast).setOnClickListener {
            LocalBroadcastManager.getInstance(this).sendBroadcast(
                Intent(ACTION_RECEIVER_1).putExtra(
                    KEY_DATA,
                    "dataToSend via LocalBroadcast"
                )
            )
        }

        findViewById<Button>(R.id.btnStickyBroadcast).setOnClickListener {
            sendStickyBroadcast(
                Intent(ACTION_RECEIVER_STICKY).putExtra(
                    KEY_DATA,
                    "dataToSend via StickyBroadcast"
                )
            )
        }
        findViewById<Button>(R.id.btnRegisterSticky).setOnClickListener {
            registerReceiver(mReceiverSticky, IntentFilter(ACTION_RECEIVER_STICKY))
        }
        findViewById<Button>(R.id.btnUnregisterSticky).setOnClickListener {
            unregisterReceiver(mReceiverSticky)
        }

        findViewById<Button>(R.id.btnGetStickyBroadcast).setOnClickListener {
            // TODO: 11/5/2021 SYSTEM STICKY broadcast value may be obtained without receiver
//            val intent = registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
//            val level = intent?.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) ?: 1
//            Log.d("MainActivity", "Receiver BatteryLevel = $level")

            val intent = registerReceiver(null, IntentFilter(ACTION_RECEIVER_STICKY))
            val stickyData = intent?.getStringExtra(KEY_DATA)
            Log.d("MainActivity", "Receiver StickyData = $stickyData")
        }
    }

    override fun onStart() {
        super.onStart()
        // TODO: 11/4/2021 Any Android component may be enabled/disabled in the same way
//        packageManager.setComponentEnabledSetting(
//            ComponentName(applicationContext, ReceiverStatic::class.java),
//            PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP
//        )

        // TODO: 11/4/2021 Priority is for order of OrderedBroadcast receival
        registerReceiver(mReceiver1, IntentFilter(ACTION_RECEIVER_1).apply { priority = -100 })
        registerReceiver(mReceiver2, IntentFilter(ACTION_RECEIVER_1).apply { priority = 100 })
        registerReceiver(mReceiverSticky, IntentFilter(ACTION_RECEIVER_STICKY))
    }

    override fun onStop() {
        super.onStop()
//        packageManager.setComponentEnabledSetting(
//            ComponentName(applicationContext, ReceiverStatic::class.java),
//            PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP
//        )

        unregisterReceiver(mReceiver1)
        unregisterReceiver(mReceiver2)
        unregisterReceiver(mReceiverSticky)
    }
}