package com.example.broadcastreceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.broadcastreceiver.MainActivity.Companion.KEY_DATA

class Receiver1 : BroadcastReceiver() {

    companion object {
        val TAG = Receiver1::class.java.simpleName
    }

    override fun onReceive(context: Context, intent: Intent) {
        val strData = intent.extras?.getString(KEY_DATA)
        Log.d(TAG, "onReceive: Receiver = ${this@Receiver1.hashCode()}")
        Log.d(TAG, "onReceive: strData = $strData, executing...")
//         TODO: 11/4/2021 Blocks UI thread
//        Thread.sleep(2000)

        //todo you can go async and hold broadcast without deleting it
//        val pendingResult = goAsync()
//        Thread {
//            Log.d(TAG, "onReceive: start Thread")
//            Thread.sleep(20000)
//            Log.d(TAG, "onReceive: end Thread")
//            pendingResult.finish()
//        }.start()

        // TODO: 11/4/2021 connect to existent service
//        peekService(context, intent)

        if (isOrderedBroadcast) {
            // TODO: 11/4/2021 To send data to next OrderedBroadcastReceiver
            setResult(1003, "Data", null)
        }
        Log.d(TAG, "onReceive: strData = $strData, executing FINISHED")
    }
}