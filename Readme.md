# BroadcastReceiver
BroadcastReceiver (receiver) is an Android component which allows you to register for system or application events. 
All registered receivers for an event are notified by the Android runtime once this event happens.
For example, a broadcast announcing that the screen has turned off, the battery is low, or a picture was captured.
Applications can also initiate broadcasts—for example, to let other applications know that some data has been downloaded to the device and is available for them to use.


# Sticky Broadcast
If we are using method sendStickyBroadcast(intent) the corresponding intent is sticky, meaning the intent you are sending stays around after broadcast is complete. 
A StickyBroadcast as the name suggests is a mechanism to read the data from a broadcast, after the broadcast is complete. 
This can be used in a scenario where you may want to check say in an Activity's **onCreate()** the value of a key in the intent before that Activity was launched.
###### Example:
```kotlin
    val intent: Intent = Intent("com.org.action");
    intent.putExtra("anIntegerKey", 0);
    sendStickyBroadcast(intent);
```

# Ordered Broadcast
Ordered broadcasts are used when you need to specify a priority for broadcast listeners.
In this example **firstReceiver** will receive broadcast always before than a **secondReceiver**:
```kotlin
    val highPriority = 2
    val lowPriority = 1
    val action = "action"

// intent filter for first receiver with high priority
    val firstFilter = IntentFilter(action)
    firstFilter.priority = highPriority
    val firstReceiver = MyReceiver()

// intent filter for second receiver with low priority
    val secondFilter = IntentFilter(action)
    secondFilter.priority = lowPriority
    val secondReceiver = MyReceiver()

// register our receivers
    context.registerReceiver(firstReceiver, firstFilter)
    context.registerReceiver(secondReceiver, secondFilter)

// send ordered broadcast
    context.sendOrderedBroadcast(Intent(action), null)
```
Furthermore broadcast receiver can abort ordered broadcast. 
In this case all receivers with lower priority will not receive a broadcast message.
```kotlin
    override fun onReceive(context: Context, intent: Intent) {
        abortBroadcast()
    }   
```


# LocalBroadcastManager
###### SenderActivity:
```kotlin
    val intent: Intent = Intent("anEvent")
    intent.putExtra("key", "This is an event")
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
```

###### ReceiverActivity:
- A concrete object for performing action when the receiver is called
```kotlin
    private val receiver: BroadcastReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
        // perform action here.
        }
    }
```
- Register a receiver.
  Put this code in the appropriate activity lifecycle callback. E.g. **onStart()**
```kotlin
    LocalBroadcastManager.getInstance(this).registerReceiver(receiver, IntentFilter("anEvent"))
```
- Unregister receiver. 
  Put this code in the appropriate activity lifecycle callback. E.g. **onStop()**
```kotlin
    LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
```